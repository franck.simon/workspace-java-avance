package exemple;

public class MainSommeTwoThreads {
	public static void main(String[] args) throws InterruptedException {
		int[] ints = Util.intArrayCreate(1_000_000_000);
		
		long result1 = Util.sum(ints);
		
		PartialSumThread th1 = new PartialSumThread(ints, 0, ints.length/2);
		PartialSumThread th2 = new PartialSumThread(ints, ints.length/2, ints.length);
		th1.setPriority(Thread.MIN_PRIORITY);
		th2.setPriority(Thread.MAX_PRIORITY);
		th1.start();
		th2.start();
		
		th1.join();
		th2.join();
		
		long result2 = th1.getPartialSum() + th2.getPartialSum();
		
		
		System.out.println(String.format("Résultat de la somme classique : %,d", result1));
		System.out.println(String.format("Résultat de la somme avec 2 threads : %,d", result2));
	}
}

package exemple;

import java.io.OutputStream;
import java.util.concurrent.ThreadLocalRandom;

public class ProducerThread extends Thread {
	private static final int NB = 20;
	private OutputStream out;

	public ProducerThread(OutputStream out) {
		this.out = out;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < NB; i++) {
				int r = ThreadLocalRandom.current().nextInt(255);
				System.out.printf("PUT %d : VALUE %d\n",i,r);
				out.write(r);
				// ajout d'une attente
				Thread.sleep(ThreadLocalRandom.current().nextInt(10) * 100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

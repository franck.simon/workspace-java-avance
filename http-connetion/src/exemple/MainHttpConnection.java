package exemple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainHttpConnection {

	public static void main(String[] args) throws IOException {
		URL urlServer = new URL("http://www.perdu.com");
		HttpURLConnection connection = (HttpURLConnection) urlServer.openConnection();
		connection.setRequestMethod("OPTIONS");
		connection.connect();
		connection.getHeaderFields().forEach((k,v)->System.out.println(k+" : "+v));
		BufferedReader reader = new BufferedReader(
									new InputStreamReader(connection.getInputStream()));

		String line;
		while((line = reader.readLine()) != null){
			System.out.println(line);
		}
		connection.disconnect();

	}

}

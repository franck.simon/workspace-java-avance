package exemple.jmx.notification;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

public class MainNotification {
	public static void main(String[] args) throws Exception {
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();

		ObjectName compteurObjectName = new ObjectName("antislashn.jmx:type=Compteur,subtype=Notification");
		CompteurMBean compteurMBean = new Compteur();
		server.registerMBean(compteurMBean, compteurObjectName);
		
		System.out.println(">>> MainNotification démarré ...");
		while(true){
			Thread.sleep(Long.MAX_VALUE);
		}
	}
}

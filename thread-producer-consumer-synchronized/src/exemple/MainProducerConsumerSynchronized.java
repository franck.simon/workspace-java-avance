package exemple;

public class MainProducerConsumerSynchronized {

	public static void main(String[] args) throws InterruptedException {
		Buffer buffer = new Buffer();
		Consumer consumer = new Consumer(buffer);
		Producer producer = new Producer(buffer);
		
		producer.start();
		consumer.start();
		
		producer.join();
	}

}

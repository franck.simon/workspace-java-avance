package exemple;

public class Foo {
	private static Foo instance;
	private int value = 3;
	
	static {
		System.out.println("code static");
		instance = new Foo();
	}
	
	{
		System.out.println("code non static");
	}
	
	public Foo() {
		System.out.println("constructeur");
	}
	
	public Foo(int i) {
		System.out.println("constructeur int");
	}
	
	public int getValeur() {
		return value;
	}
	
	public void bar(int v) {
		System.out.println("bar : "+v);
	}
}

package exemple;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class MainWatchService {

	public static void main(String[] args) throws IOException {
		// récupération d'une instance de WatchService
		WatchService watcher = FileSystems.getDefault().newWatchService();
		
		// enregistrement auprès du répertoire
		final Path dir = Paths.get("/home/franck/tmp");
		WatchKey key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
										StandardWatchEventKinds.ENTRY_DELETE,
										StandardWatchEventKinds.ENTRY_MODIFY);
		
		System.out.println("=== START");
		while(true) {
			try {
				key = watcher.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(key!=null) {
				for(final WatchEvent<?> evt : key.pollEvents()) {
					final Path name = (Path) evt.context();
					System.out.format(evt.kind() + " " + "%s\n", name);
				}
				key.reset();
			}
		}
	}

}

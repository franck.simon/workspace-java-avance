package exemple;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainFileInfo {

	public static void main(String[] args) throws IOException {
		Path start = Paths.get("/home/franck/tmp");
		String filter = "*.txt";
		
		for(Path p : Files.newDirectoryStream(start)){
			System.out.println(p.getFileName()+" "+Files.isDirectory(p));
		}

	}

}

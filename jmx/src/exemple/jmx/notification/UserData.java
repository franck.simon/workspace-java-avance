package exemple.jmx.notification;

import java.io.Serializable;

public class UserData implements Serializable{
	private int oldValue;
	private int newValue;
	
	public UserData(int oldValue, int newValue) {
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	public int getOldValue() {
		return oldValue;
	}
	public int getNewValue() {
		return newValue;
	}
}

package exemple.jmx.standard;

public interface HelloMBean {
	// attributs
	String getName();
	String getColor();
	void setColor(String color);
	
	// opérations
	void sayHello();
	double add(double a, double b);
}

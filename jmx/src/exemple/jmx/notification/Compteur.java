package exemple.jmx.notification;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class Compteur extends NotificationBroadcasterSupport implements CompteurMBean {
	private static final Logger LOG = Logger.getLogger(Compteur.class.getCanonicalName());
	private int value;
	private int delay = 2000;
	private int sequenceNumber = 1;

	public Compteur() {
		automaticChange();
	}
	
	public Compteur(int delayInMs){
		this.delay = delayInMs;
		automaticChange();
	}
	
	private void automaticChange(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!Thread.currentThread().isInterrupted()) {
					try {
						Thread.sleep(delay);
						Compteur.this.setValue(++value);
					} catch (InterruptedException e) {
						LOG.log(Level.SEVERE,"Interruption du thread",e);
					}
				}
			}
		}).start();
	}
	
	@Override
	public int getValue() {
		return value;
	}

	@Override
	public void setValue(int value) {
		this.value = value;
		sendNotification();
	}
	
	private void sendNotification() {
		Notification notification = new Notification("antislashn.compteur.value", this, sequenceNumber++);
		sendNotification(notification);	
	}

	@Override
	public MBeanNotificationInfo[] getNotificationInfo(){
		MBeanNotificationInfo[] infos = new MBeanNotificationInfo[1];
		
		String[] types = new String[]{"antislashn.compteur.value"};
		String name = AttributeChangeNotification.class.getName();
		String description = "Un attribut de Compteur a changé";
		MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		infos[0] = info;
		return infos;
	}

}

package exemple;

public class MainThreadSimple {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Thread du main : " + Thread.currentThread().getId());
		Thread thread = new MonThread();
		thread.start();
		
		Thread th1 = new Thread(new MonRunnable());
		th1.start();
//		Thread.sleep(3_000);
		
		thread.join();
		th1.join();
		System.out.println("Fin main");
//		thread.start();
	}

}

class MonThread extends Thread{
	@Override
	public void run() {
		try {
			System.out.println("thread secondaire : " + Thread.currentThread().getId());
			Thread.sleep(3_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Fin du thread");
	}
}


class MonRunnable implements Runnable{

	@Override
	public void run() {
		try {
			System.out.println("runnable : "+Thread.currentThread().getId());
			Thread.sleep(6_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Fin du Runnable");
		
	}
	
}
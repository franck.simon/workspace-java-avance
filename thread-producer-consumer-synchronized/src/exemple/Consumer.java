package exemple;

import java.util.concurrent.ThreadLocalRandom;

public class Consumer extends Thread {
	private Buffer buffer;

	public Consumer(Buffer buffer) {
		this.buffer = buffer;
	}

	@Override
	public void run() {
		try {
			while (!interrupted()) {
				String data = buffer.take();
				System.out.println("TAKE : "+data);
				sleep(ThreadLocalRandom.current().nextInt(10)*100);
			}
		} catch (Exception e) {
			interrupt();
		}
	}
}

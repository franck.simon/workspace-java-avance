package exemple;

public class TraitementLong implements IProcess {

	@Override
	public int compute() {
		System.out.println("Début TraitementLong.compute()");
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Fin TraitementLong.compute()");
		return 42;
	}

	@Override
	public void foo() {
	
	}
	
	

}

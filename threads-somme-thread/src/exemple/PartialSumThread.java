package exemple;

public class PartialSumThread extends Thread {
	private long partialSum;
	private int from;
	private int to;
	private int[] array;
	

	public PartialSumThread(int[] array, int from, int to) {
		this.array = array;
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		
		for(int i=from ; i<to ; i++) {
			partialSum += array[i];
		}
		long stop = System.currentTimeMillis();
		System.out.println(String.format(">>> somme partielle %s - durée : %,d ms", this.getName(),(stop-start)));
	}

	public long getPartialSum() {
		return partialSum;
	}
}

package exemple;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

	public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException {
		Calcul calcul = new CalculImpl();
		Registry registry = LocateRegistry.createRegistry(1099);
//		System.setProperty("java.rmi.server.codebase", Calcul.class.getProtectionDomain().getCodeSource().getLocation().toString());
//		LocateRegistry.getRegistry("127.0.0.1");
		Naming.rebind("calcul", calcul);
		System.out.println("Calcul est lancé");
	}

}

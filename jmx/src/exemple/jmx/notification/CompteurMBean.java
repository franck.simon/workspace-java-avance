package exemple.jmx.notification;

public interface CompteurMBean {

	public abstract int getValue();

	public abstract void setValue(int value);

}
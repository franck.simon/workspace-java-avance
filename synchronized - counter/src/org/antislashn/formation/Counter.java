package org.antislashn.formation;

public class Counter {
	private Object m1 = new Object();
	private long value;
	
	public void increment() {
		// code
		synchronized (m1) {
			value = value + 1;
			
		}

		// code
	}
	
	public long getValue() {

			return value;

	}
}

package exemple;

public class MainSommeClassique {

	public static void main(String[] args) {
		int[] entiers = Util.intArrayCreate(100_000_000);
		long result = Util.sum(entiers);
		System.out.println(String.format("Résultat de la somme classique : %,d", result));
	}

}

package exemple.jms.json;

public class Contact {
	private String nom;
	private int age;
	
	public Contact() {
		// rien à faire
	}
	
	
	public Contact(String nom, int age) {
		this.nom = nom;
		this.age = age;
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Contact [nom=" + nom + ", age=" + age + "]";
	}

}

package exemple;

import java.util.concurrent.ThreadLocalRandom;

public class Producer extends Thread{
	private Buffer buffer;
	
	public Producer(Buffer buffer) {
		this.buffer = buffer;
	}
	
	@Override
	public void run(){
		for(int i=0 ; i<20 ; i++){
			System.out.println("PUT : item "+i);
			buffer.put("item "+i);
			try {
				sleep(ThreadLocalRandom.current().nextInt(10)*100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

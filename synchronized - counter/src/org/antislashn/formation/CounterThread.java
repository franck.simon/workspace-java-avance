package org.antislashn.formation;

public class CounterThread extends Thread {
	private Counter counter;
	private int nbIncrements;
	
	public CounterThread(Counter counter, int nbIncrements) {
		this.counter = counter;
		this.nbIncrements = nbIncrements;
	}
	
	@Override
	public void run() {
		for(int i=0 ; i<nbIncrements; i++) {
			counter.increment();
		}
	}
}	

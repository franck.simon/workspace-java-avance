package exemple.demos;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class TestListNumberStream {

	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
		
//		List<Integer> nbs = numbers.stream().peek(System.out::println).collect(Collectors.toList());
//		System.out.println(nbs);
//		List<Integer> twoEvenSquares = 
//		    numbers.stream()
//		           .filter(n -> {
//		                    //System.out.println("filtering " + n); 
//		                    return n % 2 == 0;
//		                  })
//		           .map(n -> {
//		                    //System.out.println("mapping " + n);
//		                    return n * n;
//		                  })
//		           .limit(2)
//		           .collect(Collectors.toList());
//		System.out.println(twoEvenSquares);
		
		
		List<Integer> test = numbers.stream()
				.filter(t -> t>4)
				.map(t -> t*2)			// multiplication par 2
				.collect(toList());
		
		int somme = numbers.stream()
				.reduce(10,(a,b) -> (a+b)); // somme des éléments, 0 : valeur de départ
		System.out.println(somme);
		
		//System.out.println(Arrays.deepToString(test.toArray(new Integer[] {})));
		
		IntSummaryStatistics iss = numbers.stream().mapToInt(Integer::intValue).summaryStatistics();
		System.out.println("somme : "+iss.getSum());
		System.out.println("min : "+iss.getMin());
		System.out.println("max : "+iss.getMax());
		System.out.println("moyenne : "+iss.getAverage());
		
		IntStream intStream = IntStream.range(10, 30).filter(n -> n%2 == 0);		
		intStream.forEach(System.out::println);
//		
//		Stream<Integer> ints = Stream.iterate(10, e->e+6);
//		ints.forEach(System.out::println);
		

	}

}

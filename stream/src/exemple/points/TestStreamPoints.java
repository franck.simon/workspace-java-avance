package exemple.points;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class TestStreamPoints {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Random rnd = new Random();
		List<Point> points = new ArrayList<>();
		for(int i = 0; i<10 ; i++) {
			points.add(new Point(rnd.nextInt(100),rnd.nextInt(100)));
		}
		System.out.println(points);
		List<Integer> xs = points.stream()
								.map(Point::getX)
								.filter(x->x%2 == 0)
								.sorted()
								.collect(Collectors.toList());
		
		System.out.println(xs);
	}

}

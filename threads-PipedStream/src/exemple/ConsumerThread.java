package exemple;

import java.io.InputStream;

public class ConsumerThread extends Thread{
	private InputStream in;
	
	public ConsumerThread(InputStream in) {
		this.in = in;
	}

	@Override
	public void run() {
		int i = 0;
		try {
		while(true) {
			int r = in.read();
			System.out.printf("TAKE %d : VALUE %d\n",i++,r);
		}
		}catch (Exception e) {
			// le pipe sera brisé si le producteur est mort
			e.printStackTrace();		
		}
	}
}

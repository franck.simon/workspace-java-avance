package exemple;

public class SleepRunnable implements Runnable {
	
	@Override
	public void run() {
		long id = Thread.currentThread().getId();
		System.out.printf("SleepRunnable - Début de traitement %d\n",id);
		while(!Thread.interrupted()){
			try {
				Thread.sleep(10_000);
			} catch (InterruptedException e) {
				System.out.printf(">>> CATCH thread %d - status  : %b\n",id,Thread.currentThread().isInterrupted());
				Thread.currentThread().interrupt();
			}
		}
		System.out.printf(">>> FIN EXECUTION SleepRunnable %d - status  : %b\n",id,Thread.currentThread().isInterrupted());
	}

}

package exemple.socket.time.simple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TimeClient {

	private String hostName;
	private int port;
	private Socket client;
	
	public TimeClient(String hostName,int port){
		this.hostName = hostName;
		this.port = port;
	}
	
	public void connect() throws UnknownHostException, IOException{
		client = new Socket(hostName,port);
		System.out.println("=== Client connecté");
		BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
		writer.write("HELLO\n\n");
		writer.flush();
		String reponse = reader.readLine();
		System.out.println("=== "+reponse);
		client.close();
	}
	
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		TimeClient client = new TimeClient("127.0.0.1", 12345);
		client.connect();

	}

}

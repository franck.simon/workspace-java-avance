package org.antislashn.calcul;

import org.antislashn.calcul.impl.CalculImpl;

public interface ICalcul {
	int add(int...is);
	
	static ICalcul newInstance() {
		return new CalculImpl();
	}
}

package exemple.jmx.standard;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

public class MainStandardMBeanHello {

	public static void main(String[] args) throws MalformedObjectNameException, InterruptedException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		
		ObjectName mBeanName = new ObjectName("antislashn.jmx:type=standard");
		
		HelloMBean mBean = new Hello();
		mbs.registerMBean(mBean, mBeanName);
		
		RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
		List<String> arguments = runtimeMXBean.getInputArguments();
		System.out.println("Agent MBean démarré...");
		while(true)
			Thread.sleep(Long.MAX_VALUE);
	}

}

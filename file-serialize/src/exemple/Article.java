package exemple;

import java.io.Serializable;

public class Article implements Serializable {

	private String nom;
	private String id;
	
	public Article() {}
	
	public Article(String nom, String id) {
		this.nom = nom;
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Article [nom=" + nom + ", id=" + id + "]";
	}

}

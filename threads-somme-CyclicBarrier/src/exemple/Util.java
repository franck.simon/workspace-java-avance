package exemple;

import java.util.Random;

public class Util {
	public static long sum(int[] array) {
		long start = System.currentTimeMillis();
		
		long total = 0;
		for(int i : array) {
			total += i;
		}
		long stop = System.currentTimeMillis();
		System.out.println(String.format(">>> somme classique - durée : %,d ms", (stop-start)));
		return total;
	}
	
	public static int[] intArrayCreate(int itemsNb) {
		int[] ints = new int[itemsNb];
		Random random = new Random();
		for(int i=0 ; i<itemsNb ; i++) {
			ints[i] = random.nextInt();
		}	
		return ints;
	}
}

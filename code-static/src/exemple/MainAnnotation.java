package exemple;

public class MainAnnotation {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		Contact c = new Contact();
		System.out.println(c);
		Container.evaluer(c);
		System.out.println(c);
	}

}

package exemple;

import java.util.List;

public class TotalSumThread extends Thread {
	private long total;
	private List<PartialSumThread> threads;
	
	public TotalSumThread(List<PartialSumThread> threads) {
		this.threads = threads;
	}
	
	@Override
	public void run() {
		for(PartialSumThread th : threads) {
			total += th.getPartialSum();
		}
		System.out.println(String.format("Résultat de la somme avec %d threads : %,d",threads.size(), total));
	}

	public long getTotal() {
		return total;
	}
	
}

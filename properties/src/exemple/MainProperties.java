package exemple;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class MainProperties {

	public static void main(String[] args) throws IOException {
		String fileName = "application.properties";
		try(var in = MainProperties.class.getClassLoader().getResourceAsStream(fileName)){
			Properties props = new Properties();
			props.load(in);
			String titre = props.getProperty("titre");
			System.out.println(titre);
			String port = props.getProperty("port");
			System.out.println(port);
		}
		System.out.println(System.getProperty("user.dir"));
		System.out.println(System.getProperty("user.home"));

	}

}

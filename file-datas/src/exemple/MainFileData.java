package exemple;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainFileData {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String fileName = "donnees.data";
		
		try(var dout = new DataOutputStream(new FileOutputStream(fileName))){
			dout.writeFloat(10.1F);
			dout.writeDouble(42.42);
			dout.writeInt(84);
		}
		
		try(var din = new DataInputStream(new FileInputStream(fileName))){
			System.out.println(din.readFloat());
			System.out.println(din.readDouble());
			System.out.println(din.readInt());
		}
	}

}

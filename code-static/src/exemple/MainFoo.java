package exemple;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainFoo {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, FileNotFoundException, IOException, InstantiationException {
		String clazz = new BufferedReader(new FileReader("test.txt")).readLine();
		System.out.println(clazz);
		Class.forName(clazz).getConstructor().newInstance();
//		Class.forName("exemple.Foo");
//		Foo f1 = new Foo(1);

//		Field[] fields = Foo.class.getDeclaredFields();
//		for(Field f : fields) {
//			System.out.println(f.getName());
//		}
//		
//		Field f = Foo.class.getDeclaredField("value");
//		f.setAccessible(true);
//		f.set(f1, 10);
//		System.out.println(f1.getValeur());
//		
//		Method m = Foo.class.getDeclaredMethod("getValeur");
//		int v = (int) m.invoke(f1);
//		System.out.println(v);
//		
//		Method m1 = Foo.class.getDeclaredMethod("bar", int.class);
//		m1.invoke(f1, 20);
	}

}

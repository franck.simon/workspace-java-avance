package exemple.socket.time.simple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class TimeServer {
	private int port = 12345;
	private ServerSocket server;

	public TimeServer() throws IOException {
		init();
	}

	public TimeServer(int port) throws IOException {
		this.port = port;
		init();
	}

	private void init() throws IOException {
		server = new ServerSocket(port);
	}

	private void waitConnection() throws IOException {
		Socket sockClient = null;
		InputStream in = null;
		OutputStream out = null;
		System.out.printf("Serveur démarré %s\n", server.getLocalSocketAddress());
		while (true) {
			sockClient = server.accept();
			System.out.println(
					">>> Connexion du client " + sockClient.getInetAddress() + " [" + sockClient.getPort() + "]");
			in = sockClient.getInputStream();
			out = sockClient.getOutputStream();
			handleConnection(in, out);
			sockClient.close();
		}
	}

	private void handleConnection(InputStream in, OutputStream out) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		String hello = reader.readLine();
		System.out.println(">>> " + hello);
		if (hello.equals("HELLO"))
			writer.write(new Date().toString() + "\n");
		writer.flush();
	}

	public static void main(String[] args) throws IOException {
		TimeServer server = new TimeServer();
		server.waitConnection();
	}

}

package exemple;

public class MainDebug {

	public static void main(String[] args) {
		Foo f = a(1,"un");
		System.out.println("résultat : "+b(1,2,3));

	}
	
	static Foo a(int i, String nom) {
		return new Foo(i,nom);
	}
	
	static int b(int...is) {
		return c(is);
	}

	private static int c(int[] is) {
		int t = 0;
		for(int i : is) {
			t += i;
		}
		return t;
	}

}

class Foo{
	int i;
	String nom;
	public Foo(int i, String nom) {
		this.i = i;
		this.nom = nom;
	}
	
	
}

package exemple.jmx.standard;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class ClientStandardMBean {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		JMXServiceURL serviceURL = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://:3333/jmxrmi");
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL);
		MBeanServerConnection serverConnection = jmxConnector.getMBeanServerConnection();
		
		ObjectName objectName = new ObjectName("antislashn.jmx:type=standard");
		HelloMBean mBean = JMX.newMBeanProxy(serverConnection, objectName, HelloMBean.class);
		System.out.println(">>> "+mBean.getName());
	}

}

package exemple;

public class MainOptional {

	public static void main(String[] args) {
		Adresse a1 = new Adresse("rue de Paris","34444","Bruxelles");
		Contact c1 = new Contact("M", "LAGAFFE", "Gaston",null);
		c1.getAdresse().ifPresent(System.out::println);

	}

}

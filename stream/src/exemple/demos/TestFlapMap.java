package exemple.demos;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TestFlapMap {

	public static void main(String[] args) {
		List<List<String>> liste = Arrays.asList(
					Arrays.asList("a"),
					Arrays.asList("b")
				);
		System.out.println(liste);

		System.out.println(liste.stream().
								flatMap(Collection::stream)
								.collect(Collectors.toList()));
		
	}
	

}

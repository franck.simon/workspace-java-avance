package exemple;

import java.util.concurrent.Semaphore;

public class MainSemaphore {
	static final int NB_DAB = 4;
	static final int NB_USER = 6;
	
	public static void main(String[] args) {
		Semaphore semaphore = new Semaphore(NB_DAB);
		
		for(int i=0 ; i<NB_USER ; i++) {
			DABUserThread th = new DABUserThread(semaphore, "user "+i);
			th.start();
		}
		
	}
}

package exemple.jmx.standard;

public class Hello implements HelloMBean {
	private String name ="Toto Standard MBean";
	private String color = "vert";
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getColor() {
		return color;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void sayHello() {
		System.out.println(">>> Standard MBean "+name+" - "+color);

	}

	@Override
	public double add(double a, double b) {
		return a+b;
	}

}

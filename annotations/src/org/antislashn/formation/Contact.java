package org.antislashn.formation;

public final class Contact {
	@Inject(nom="Toto")	private String nom;
	@Inject(age=14) private int age;
	
	
	public Contact() {}
	
	
	public Contact(String nom, int age) {
		this.nom = nom;
		this.age = age;
	}
	
	public Contact(Contact c) {
		this.nom = new String(c.nom);
		this.age = c.age;
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}


	@Override
	public String toString() {
		return "Contact [nom=" + nom + ", age=" + age + "]";
	}
	
	
	
}

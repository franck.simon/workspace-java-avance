package exemple;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class ProxyTest {

	public static void main(String[] args) {
//		IProcess process = new TraitementLong();
	
		InvocationHandler handler = new ProcessInvocationHandler(new TraitementLong());
		IProcess process = (IProcess) Proxy.newProxyInstance(IProcess.class.getClassLoader(), new Class[]{IProcess.class}, handler); 
		int r = process.compute();
		System.out.println("Résultat => "+r);
		process.foo();
	}


}

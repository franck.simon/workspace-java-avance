package org.antislashn.formation;

import java.lang.reflect.Field;

public class Injector {
	public static void inject(Object o) throws IllegalArgumentException, IllegalAccessException{
		Field[] fields = o.getClass().getDeclaredFields();
		
		for(Field f : fields){
			if(f.isAnnotationPresent(Inject.class)){
				Inject inject = f.getAnnotation(Inject.class);
				f.setAccessible(true);
				if(f.getType() == int.class){
					f.setInt(o, inject.age());
				}
				else if(f.getType() == String.class){
					f.set(o, inject.nom());
				}
			}
		}
	}
}

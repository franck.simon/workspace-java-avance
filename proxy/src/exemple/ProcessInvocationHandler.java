package exemple;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProcessInvocationHandler implements InvocationHandler {
	private IProcess process;
	
	
	public ProcessInvocationHandler(IProcess process) {
		this.process = process;
	}


	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		System.out.println("nom de la méthode "+method.getName());
		long start = System.currentTimeMillis();
		Object r = method.invoke(process, args);
		long end = System.currentTimeMillis();
		System.out.println("Process time : "+ (end-start));
		return r;
	}

}

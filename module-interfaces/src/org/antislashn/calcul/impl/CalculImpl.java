package org.antislashn.calcul.impl;

import org.antislashn.calcul.ICalcul;

public class CalculImpl implements ICalcul{

	@Override
	public int add(int... is) {
		int total = 0;
		for(int i:is) {
			total += i;
		}
		return total;
	}

}

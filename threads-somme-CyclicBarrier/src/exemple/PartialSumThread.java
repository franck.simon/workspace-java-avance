package exemple;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;

public class PartialSumThread extends Thread {
	private CyclicBarrier barrier;
	private long partialSum = 0;
	private int from;
	private int to;
	private int[] array;

	public PartialSumThread(CyclicBarrier barrier, int[] array, int from, int to) {
		this.barrier = barrier;
		this.array = array;
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() {
		for (int i = from; i < to; i++) {
			partialSum += array[i];
		}
		try {
			sleep(ThreadLocalRandom.current().nextLong(10) * 100);
			// attente de tous les autres threads
			barrier.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	public long getPartialSum() {
		return partialSum;
	}
}

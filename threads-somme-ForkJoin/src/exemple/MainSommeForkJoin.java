package exemple;

import java.util.concurrent.ForkJoinPool;

public class MainSommeForkJoin {
	private static final int NB_COUNT = 100;
	
	public static void main(String[] args) {
		int[] ints = Util.intArrayCreate(10_000_000);
		
		ForkJoinPool pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		
		ArraySumCompute.setArray(ints);
		ArraySumCompute.setNbInts(NB_COUNT);
		ArraySumCompute arraySumCompute = new ArraySumCompute(0, ints.length);
		
		long start = System.currentTimeMillis();
		long r = pool.invoke(arraySumCompute);
		long stop = System.currentTimeMillis();
		System.out.println(String.format(">>> somme fork join - durée : %,d ms - somme : %d", (stop-start),r));
		
		long r1 = Util.sum(ints);
		
		
	}
}

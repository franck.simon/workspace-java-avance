package exemple;

import java.lang.reflect.Field;

public class Container {
	
	public static void evaluer(Object o) throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = o.getClass().getDeclaredFields();
		for(Field f : fields) {
			if(f.isAnnotationPresent(ChangerValeur.class)) {
				ChangerValeur cv = f.getAnnotation(ChangerValeur.class);
				String v = cv.valeur();
				f.setAccessible(true);
				f.set(o, v);
			}
		}
		
	}

}

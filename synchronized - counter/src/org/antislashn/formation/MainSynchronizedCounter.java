package org.antislashn.formation;

import java.util.ArrayList;
import java.util.List;

public class MainSynchronizedCounter {

	public static void main(String[] args) throws InterruptedException {
		Counter counter = new Counter();
		List<Thread > threads = new ArrayList<>();
		int nbThreads = 1_000;
		int nbIncrements = 100_000;
		
		for(int i=0 ; i<nbThreads ; i++) {
			Thread th = new CounterThread(counter, nbIncrements);
			threads.add(th);
			th.start();
		}
		
		for(Thread th : threads) {
			th.join();
		}
		
		System.out.printf("Valeur attendue : %,d, valeur obtenue : %,d",nbThreads*nbIncrements,counter.getValue());	

	}

}

package exemple.jmx.notification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class ClientNotification {

	public static void main(String[] args) throws Exception {
		JMXServiceURL serviceURL = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://:3333/jmxrmi");
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL);
		MBeanServerConnection serverConnection = jmxConnector.getMBeanServerConnection();
		
		ObjectName objectName = new ObjectName("antislashn.jmx:type=Compteur,subtype=Notification");
		CompteurMBean compteurMBean = JMX.newMBeanProxy(serverConnection, objectName, CompteurMBean.class, true);
//		String typeLog = "email";
		serverConnection.addNotificationListener(objectName, new CompteurListener(), null, null);
		
		System.out.println(">>> ClientNotification démarré ...");
		while(true){
			Thread.sleep(Long.MAX_VALUE);
		}

	
	}

}

class CompteurListener implements NotificationListener{
	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println(">>> Notification "+notification.getSource());
//		String typeLog = (String) handback;
//		if(typeLog.equals("console")){
//			System.out.println(">>> Notification sur la console "+notification.getSource());
//		}else if(typeLog.equals("email")){
//			// Envoi d'un email
//		}
//		if(notification.getUserData()!=null){
//			UserData userData = (UserData) notification.getUserData();
//			System.out.println("\t oldValue == "+userData.getOldValue()+" - newValue == "+userData.getNewValue());
//		}
		
	}
}

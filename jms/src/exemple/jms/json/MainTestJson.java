package exemple.jms.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainTestJson {
	public static void main(String[] args) throws JsonProcessingException {
		Contact c1 = new Contact("Gaston",22);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(c1);
		
		Contact c2 = mapper.readValue(json, Contact.class);
		System.out.println(json);
		System.out.println(c2);
	}
}

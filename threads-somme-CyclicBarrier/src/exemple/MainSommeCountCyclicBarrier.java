package exemple;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

public class MainSommeCountCyclicBarrier {
	private static final int NB_COUNT = 100;
	public static void main(String[] args) throws InterruptedException {
		int[] ints = Util.intArrayCreate(100_000_000);	
		long result1 = Util.sum(ints);
		
		List<PartialSumThread> threads = new ArrayList<>();
		final CyclicBarrier barrier = new CyclicBarrier(NB_COUNT, new TotalSumThread(threads));
		
		int shrunk = ints.length/NB_COUNT;
		
		for(int i=0 ; i<NB_COUNT ; i++) {
			PartialSumThread th = new PartialSumThread(barrier, ints, shrunk*i, shrunk*(i+1));
			th.start();
			threads.add(th);
		}
		
		System.out.println(String.format("Résultat de la somme classique : %,d", result1));

	}

}

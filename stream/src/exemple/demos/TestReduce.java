package exemple.demos;

import java.util.Arrays;

public class TestReduce {

	public static void main(String[] args) {
		int[] entiers = {1,2,3,4,5};
		
		int somme = Arrays.stream(entiers).reduce(0,(a,b)->a+b);
		System.out.println(somme);

	}

}

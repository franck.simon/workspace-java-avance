package exemple;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class TestCalculRMI {


	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		Calcul calcul = (Calcul) Naming.lookup("rmi://127.0.0.1:1099/calcul");
		System.out.println("Resultat = "+calcul.add(2, 2));
	}

}

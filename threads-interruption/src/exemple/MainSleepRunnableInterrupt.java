package exemple;

import java.util.Scanner;

public class MainSleepRunnableInterrupt {

	public static void main(String[] args) {
		Thread t1 = new Thread(new SleepRunnable());
		Thread t2 = new Thread(new SleepRunnable());

		t1.start();
		t2.start();

		Scanner in = new Scanner(System.in);
		System.out.println("Entrez le numéro du thread à arrêter (1 ou 2): ");
		while (!t1.getState().equals(Thread.State.TERMINATED) && !t2.getState().equals(Thread.State.TERMINATED)) {
			int num = in.nextInt();
			switch (num) {
			case 1:
				t1.interrupt();
				break;
			case 2:
				t2.interrupt();
				break;
			}
		}
		in.close();
	}

}

Pour accéder au serveur JMX, il faut ajouter les paramètres suivants à la configuration de run

-Dcom.sun.management.jmxremote.port=3333
-Dcom.sun.management.jmxremote.ssl=false
-Dcom.sun.management.jmxremote.authenticate=false
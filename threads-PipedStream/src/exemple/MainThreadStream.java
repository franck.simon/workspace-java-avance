package exemple;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class MainThreadStream {

	public static void main(String[] args) throws IOException, InterruptedException {
		PipedInputStream in = new PipedInputStream();
		PipedOutputStream out = new PipedOutputStream(in);
		
		Thread producer = new ProducerThread(out);
		Thread consumer = new ConsumerThread(in);
		
		producer.start();
		consumer.start();
		
		producer.join();
		consumer.join();
		
		out.close();
		in.close();
		System.out.println("END");
	}

}

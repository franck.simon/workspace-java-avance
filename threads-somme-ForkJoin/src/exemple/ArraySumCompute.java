package exemple;

import java.util.concurrent.RecursiveTask;

public class ArraySumCompute extends RecursiveTask<Long> {
	private static int nbInts;
	private static int[] array;
	private int iStart;
	private int iEnd;
	
	private static int nb = 0;
	
	public ArraySumCompute(int iStart, int iEnd) {
		this.iStart = iStart;
		this.iEnd = iEnd;
	}
	

	@Override
	protected Long compute() {
		if((iEnd - iStart) > nbInts) {
			ArraySumCompute c1 = new ArraySumCompute(iStart, (iEnd-iStart)/2+iStart);
			ArraySumCompute c2 = new ArraySumCompute((iEnd-iStart)/2+iStart, iEnd);
			c1.fork();
			c2.fork();
			
			long r = c1.join();
			r += c2.join();
			
			return r;			
		}
		long partialSum = 0; 
		for(int i=iStart ; i<iEnd ; i++) {
			partialSum += array[i];
		}
		return partialSum;
	}



	public static void setNbInts(int nbInts) {
		ArraySumCompute.nbInts = nbInts;
	}


	public static void setArray(int[] array) {
		ArraySumCompute.array = array;
	}



}

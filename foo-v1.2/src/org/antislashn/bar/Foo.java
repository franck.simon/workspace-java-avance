package org.antislashn.bar;

// version 1.2
public class Foo {
	public void sayHello() {
		System.out.println("Hello - Foo v1.2");
	}
	
	public void sayHello(String name) {
		System.out.println("Hello - Foo v1.2, "+name);
	}

}

package exemple.jms.consumer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ExempleJmsQueueSyncConsumer {

	public static void main(String[] args) throws NamingException, JMSException {
		Context ctx = new InitialContext();
		
		ConnectionFactory factory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
		
		Connection connection = factory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
		Destination destination = (Destination) ctx.lookup("A");
		
		MessageConsumer consumer = session.createConsumer(destination);
		TextMessage message = (TextMessage) consumer.receive();
		System.out.println("=== Réception du message : "+message.getText());
		session.close();
		connection.close();

	}

}

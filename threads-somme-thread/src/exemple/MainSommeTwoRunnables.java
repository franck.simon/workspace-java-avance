package exemple;

public class MainSommeTwoRunnables {
	public static void main(String[] args) throws InterruptedException {
		int[] ints = Util.intArrayCreate(1_000_000_000);
		
		long result1 = Util.sum(ints);
		
		PartialSumRunnable r1 = new PartialSumRunnable(ints, 0, ints.length/2);
		PartialSumRunnable r2 = new PartialSumRunnable(ints, ints.length/2, ints.length);
		
		Thread th1 = new Thread(r1);
		Thread th2 = new Thread(r2);
		
		th1.start();
		th2.start();
		
		th1.join();
		th2.join();
		
		long result2 = r1.getPartialSum() + r2.getPartialSum();
		
		
		System.out.println(String.format("Résultat de la somme classique : %,d", result1));
		System.out.println(String.format("Résultat de la somme avec 2 threads : %,d", result2));
	}
}

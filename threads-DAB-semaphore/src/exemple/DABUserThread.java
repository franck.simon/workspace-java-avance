package exemple;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

public class DABUserThread extends Thread {
	private final Semaphore semaphore;
	private final String user;

	public DABUserThread(Semaphore semaphore, String user) {
		this.semaphore = semaphore;
		this.user = user;
	}

	@Override
	public void run() {
		try {
			// l'utilisateur marche jusqu'au DAB
			sleep(ThreadLocalRandom.current().nextInt(5) * 100);
			System.out.printf("%s arrive - %d jetons disponibles\n", user, semaphore.availablePermits());
			semaphore.acquire();
			System.out.printf("retrait en cours pour %s - nb de jetons %d\n", user, semaphore.availablePermits());
			// temps passé à retirer l'argent
			sleep(ThreadLocalRandom.current().nextInt(5) * 1_000);
			System.out.printf("fin de retrait pour %s\n", user);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			semaphore.release();
		}
	}
}

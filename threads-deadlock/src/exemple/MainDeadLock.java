package exemple;

public class MainDeadLock {

	public static void main(String[] args) throws InterruptedException {
		DeadLockThread th1 = new DeadLockThread();
		DeadLockThread th2 = new DeadLockThread();
		th1.setOtherThread(th2);
		th2.setOtherThread(th1);
		
		th1.start();
		th2.start();
		
		th1.join();
		th2.join();

	}

}

package exemple.socket.time.thread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.logging.Logger;

public class TimeServer {
	static final Logger LOG = Logger.getLogger(TimeServer.class.getCanonicalName());
	private int port = 12345;
	private ServerSocket server;

	public TimeServer() throws IOException {
		init();
	}

	public TimeServer(int port) throws IOException {
		this.port = port;
		init();
	}

	private void init() throws IOException {
		server = new ServerSocket(port);
	}

	private void waitConnection() throws IOException {
		Socket sockClient = null;
		InputStream in = null;
		OutputStream out = null;
		LOG.info(String.format("Serveur démarré %s\n", server.getLocalSocketAddress()));
		while (!Thread.currentThread().isInterrupted()) {
			sockClient = server.accept();
			LOG.info(">>> Connexion du client " + sockClient.getInetAddress() + " [" + sockClient.getPort() + "]");
			TimeClient timeClient = new TimeClient(sockClient);
			timeClient.start();
		}
	}
	public static void main(String[] args) throws IOException {
		TimeServer server = new TimeServer();
		server.waitConnection();
	}
	
	private class TimeClient extends Thread{
		private Socket socket;

		TimeClient(Socket socket) {
			this.socket = socket;
		}
		
		void handle() throws IOException {
			try(BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))){
				String hello = reader.readLine();
				System.out.println(">>> " + hello);
				if (hello.equals("HELLO"))
					writer.write(new Date().toString() + "\n");
				writer.flush();
			}catch(IOException e) {
				e.printStackTrace();
			}finally {
				socket.close();
			}
		}
		
		@Override
		public void run() {
			try {
				handle();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

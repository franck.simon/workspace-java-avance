package exemple;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculImpl extends UnicastRemoteObject implements Calcul {

	protected CalculImpl() throws RemoteException {
		super();
	}

	@Override
	public int add(int a, int b) throws RemoteException {
		return a + b;
	}


}

package exemple;

import java.util.concurrent.ThreadLocalRandom;

public class DeadLockThread extends Thread{
	private Thread otherThread;
	
	@Override
	public void run() {
		try {
			Thread.sleep(ThreadLocalRandom.current().nextInt(10)*100);
			otherThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setOtherThread(Thread otherThread) {
		this.otherThread = otherThread;
	}

}

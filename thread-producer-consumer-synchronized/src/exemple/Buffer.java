package exemple;

public class Buffer {
	private String[] datas = new String[10];
	private int indexIn = 0;	// position du prochain ajout
	private int indexOut = 0;	// position du prochain retrait
	private int nbElements = 0;	// nombre d'éléments présent
	private Object lock = new Object();

	public void put(String data) {
		synchronized (lock) {
			if (nbElements == data.length()) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			nbElements++;
			indexIn = (indexIn+1) % datas.length;
			datas[indexIn] = data;
			lock.notify();
		}
	}

	public String take() {
		synchronized (lock) {
			if (nbElements==0) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			nbElements--;
			indexOut = (indexOut + 1) % datas.length;
			lock.notify();
			return datas[indexOut];
		}
	}
}

package exemple;

import java.util.concurrent.CountDownLatch;

public class PartialSumThread extends Thread {
	private CountDownLatch latch;
	private long partialSum=0;
	private int from;
	private int to;
	private int[] array;
	

	public PartialSumThread(CountDownLatch latch ,int[] array, int from, int to) {
		this.latch = latch;
		this.array = array;
		this.from = from;
		this.to = to;
	}

	@Override
	public void run() {
		for(int i=from ; i<to ; i++) {
			partialSum += array[i];
		}
		latch.countDown();
	}

	public long getPartialSum() {
		return partialSum;
	}
}

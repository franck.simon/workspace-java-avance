package exemple;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class MainSommeCountDownLatch {
	private static final int NB_COUNT = 100;
	public static void main(String[] args) throws InterruptedException {
		int[] ints = Util.intArrayCreate(100_000_000);	
		long result1 = Util.sum(ints);
		
		final CountDownLatch latch = new CountDownLatch(NB_COUNT);
		
		List<PartialSumThread> threads = new ArrayList<>();
		int shrunk = ints.length/NB_COUNT;
		
		for(int i=0 ; i<NB_COUNT ; i++) {
			PartialSumThread th = new PartialSumThread(latch, ints, shrunk*i, shrunk*(i+1));
			th.start();
			threads.add(th);
		}
		
		latch.await();
		
		long result2 = 0;
		for(PartialSumThread th : threads) {
			result2 += th.getPartialSum();
		}
		
		System.out.println(String.format("Résultat de la somme classique : %,d", result1));
		System.out.println(String.format("Résultat de la somme avec %d threads : %,d",NB_COUNT, result2));

	}

}

package exemple;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainFileSerialize {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String fileName = "datas.ser";
		
		try(var out = new ObjectOutputStream(new FileOutputStream(fileName))){
			Article[] articles = {new Article("a1", "id1"),new Article("a1", "id1")};
			out.writeObject(articles);
		}
		
		
		try(var in = new ObjectInputStream(new FileInputStream(fileName))){
			Article[] articles = (Article[]) in.readObject();
			for(var a : articles) {
				System.out.println(a);
			}
		}

	}

}
